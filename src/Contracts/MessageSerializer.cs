﻿using System.Text.Json;

namespace Contracts;

public class MessageSerializer<TMessage> : IMessageSerializer<TMessage>, IMessageDeserializer<TMessage>
{
    public ReadOnlyMemory<byte> Serialize(TMessage message)
    {
        return JsonSerializer.SerializeToUtf8Bytes(message);
    }

    public TMessage Deserialize(ReadOnlySpan<byte> messageBytes)
    {
        var message = JsonSerializer.Deserialize<TMessage>(messageBytes);

        if (message is null)
            throw new InvalidOperationException($"Cannot deserialize message to {nameof(TMessage)}");

        return message;
    }
}
