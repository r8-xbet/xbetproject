﻿namespace Contracts.Models;

public record BetMessage(long ClientId, long SportEventId, decimal Odd, string TeamName, decimal Amount, DateTime Time);
