﻿using System.Reflection;
using Microsoft.EntityFrameworkCore;
using SportEventsService.Models;

namespace SportEventsService.Infrastructure.Database;

public class AppDbContext : DbContext
{
    public required DbSet<SportEvent> SportEvents { get; set; }

    public AppDbContext(DbContextOptions<AppDbContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(
            Assembly.GetExecutingAssembly());
    }
}
