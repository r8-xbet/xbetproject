﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SportEventsService.Models;

namespace SportEventsService.Infrastructure.Database.Configuration;

internal class SportEventConfiguration : IEntityTypeConfiguration<SportEvent>
{
    public void Configure(EntityTypeBuilder<SportEvent> builder)
    {
        builder
            .Property(x => x.Id)
            .ValueGeneratedOnAdd();
    }
}
