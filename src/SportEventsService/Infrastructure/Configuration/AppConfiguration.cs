﻿namespace SportEventsService.Infrastructure.Configuration;

internal static class AppConfiguration
{
    public static string PostgresConnectionString { get; private set; } = null!;

    public static void RegisterSettings(this IServiceCollection services, IConfiguration configuration)
    {
        var postgresConnectionString = configuration.GetConnectionString("Postgres") ?? throw new InvalidOperationException("Connection for DB was not found");

        PostgresConnectionString = postgresConnectionString;
    }
}
