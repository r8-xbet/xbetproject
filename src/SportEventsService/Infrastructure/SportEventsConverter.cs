﻿using Google.Protobuf.WellKnownTypes;
using SportEventsContractsService;

namespace SportEventsService.Infrastructure;

public static class SportEventsConverter
{
    public static IList<SportEvent> ToDto(this IList<Models.SportEvent> sportEvents) =>
        sportEvents.Select(sportEvent => new SportEvent
            {
                Id = sportEvent.Id,
                EventName = sportEvent.EventName,
                TeamOne = sportEvent.TeamOne,
                TeamTwo = sportEvent.TeamTwo,
                Time = sportEvent.Time.ToTimestamp()
            })
            .ToList();
}
