﻿using Microsoft.EntityFrameworkCore;
using SportEventsService.Adapters.Grpc;
using SportEventsService.Infrastructure.Configuration;
using SportEventsService.Infrastructure.Database;

var builder = WebApplication.CreateBuilder(args);

builder.Services.RegisterSettings(builder.Configuration);

builder.Services.AddGrpc();
builder.Services.AddGrpcReflection();

builder.Services.AddDbContext<AppDbContext>((_, options) => options
    .UseNpgsql(AppConfiguration.PostgresConnectionString));

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.MapGrpcReflectionService();
}

app.MapGrpcService<SportEventsServiceProvider>();

await app.MigrateDatabase();

await app.RunAsync();
