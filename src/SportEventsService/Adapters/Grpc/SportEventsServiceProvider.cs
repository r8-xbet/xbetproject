﻿using Google.Protobuf.Collections;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using SportEventsContractsService;
using SportEventsService.Infrastructure;
using SportEventsService.Infrastructure.Database;

namespace SportEventsService.Adapters.Grpc;

public class SportEventsServiceProvider : SportEventsApiService.SportEventsApiServiceBase
{
    private readonly AppDbContext _appDbContext;
    private readonly ILogger<SportEventsServiceProvider> _logger;
    readonly string[] _sportEvents = ["Бокс", "Теннис", "Футбол", "Баскетбол"];
    readonly string[] _teamNames = ["Команда А", "Ренджеры", "Хорошие ребята", "Не очень ребята"];

    public SportEventsServiceProvider(AppDbContext appDbContext,ILogger<SportEventsServiceProvider> logger)
    {
        _appDbContext = appDbContext ?? throw new ArgumentNullException(nameof(appDbContext));
        _logger = logger;
    }

    public override async Task<RepeatedField<SportEventsResponse>> GetSportEvents(Empty request, IServerStreamWriter<SportEventsResponse> responseStream, ServerCallContext context)
    {
        _logger.LogInformation("Method {Method} was started", nameof(GetSportEvents));

        while (true)
        {
            var sportEvents = Enumerable.Range(1, 3).Select(index =>
                Models.SportEvent.Create(
                    _sportEvents[Random.Shared.Next(_sportEvents.Length)],
                    _teamNames[Random.Shared.Next(_teamNames.Length)],
                    _teamNames[Random.Shared.Next(_teamNames.Length)],
                    DateTime.UtcNow.AddMinutes(index))).ToArray();

            await _appDbContext.SportEvents.AddRangeAsync(sportEvents, context.CancellationToken);
            await _appDbContext.SaveChangesAsync(context.CancellationToken);

            var outputEvents = sportEvents.ToDto();

            var response = new SportEventsResponse { SportEvents = { outputEvents } };

            await responseStream.WriteAsync(response);

            _logger.LogInformation("Method {Method} sent response: {Response}", nameof(GetSportEvents), response);

            await Task.Delay(TimeSpan.FromSeconds(5));
        }


    }
}
