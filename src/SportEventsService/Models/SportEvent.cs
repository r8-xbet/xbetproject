﻿namespace SportEventsService.Models;

public class SportEvent
{
    protected SportEvent(string eventName, string teamOne, string teamTwo, DateTime time)
    {
        EventName = eventName;
        Time = time;
        TeamOne = teamOne;
        TeamTwo = teamTwo;
    }

    /// <summary>
    /// Идентификатор спортивного события
    /// </summary>
    public long Id { get; init; }

    /// <summary>
    /// Наименование спортивного события
    /// </summary>
    public string EventName { get; set; }

    /// <summary>
    /// Команда 1
    /// </summary>
    public string TeamOne { get; set; }

    /// <summary>
    /// Команда 2
    /// </summary>
    public string TeamTwo { get; set; }

    /// <summary>
    /// Дата и время матча
    /// </summary>
    public DateTime Time { get; set; }

    public static SportEvent Create(string eventName, string teamOne, string teamTwo, DateTime time) =>
        new(eventName, teamOne, teamTwo, time);
}
