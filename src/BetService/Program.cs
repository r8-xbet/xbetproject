﻿using BetService.Adapters;
using BetService.Adapters.Grpc;
using BetService.Applications;
using BetService.Infrastructure.Configuration;
using BetService.Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using OddsContractsService;
using SportEventsContractsService;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddGrpc();
builder.Services.AddGrpcReflection();

builder.Services.AddHostedService<SportEventsLoaderHostedService>();
builder.Services.AddHostedService<BetConsumerService>();

builder.Services.RegisterSettings(builder.Configuration);

builder.Services.AddGrpcClient<OddsApiService.OddsApiServiceClient>(o =>
{
    o.Address = AppConfiguration.OddsServiceUri;
});

builder.Services.AddGrpcClient<SportEventsApiService.SportEventsApiServiceClient>(o =>
{
    o.Address = AppConfiguration.SportEventsServiceUri;
});

builder.Services.AddDbContext<AppDbContext>((_, options) => options
    .UseNpgsql(AppConfiguration.PostgresConnectionString));

builder.Services.AddScoped<IDbProvider, DbProvider>();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.MapGrpcReflectionService();
}

app.MapGrpcService<BetsServiceProvider>();

await app.MigrateDatabase();

await app.RunAsync();
