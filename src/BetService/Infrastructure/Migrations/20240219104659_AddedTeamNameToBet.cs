﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BetService.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class AddedTeamNameToBet : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TeamName",
                table: "Bets",
                type: "text",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TeamName",
                table: "Bets");
        }
    }
}
