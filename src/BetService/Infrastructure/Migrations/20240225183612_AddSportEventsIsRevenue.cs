﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BetService.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class AddSportEventsIsRevenue : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsRevenue",
                table: "SportEvents",
                type: "boolean",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsRevenue",
                table: "SportEvents");
        }
    }
}
