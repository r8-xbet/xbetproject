﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BetService.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class AddedTeamNames : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TeamOne",
                table: "SportEvents",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "TeamTwo",
                table: "SportEvents",
                type: "text",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TeamOne",
                table: "SportEvents");

            migrationBuilder.DropColumn(
                name: "TeamTwo",
                table: "SportEvents");
        }
    }
}
