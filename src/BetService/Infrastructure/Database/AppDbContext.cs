﻿using System.Reflection;
using BetService.Models;
using Microsoft.EntityFrameworkCore;

namespace BetService.Infrastructure.Database;

public class AppDbContext : DbContext
{
    public required DbSet<Bet> Bets { get; set; }
    public required DbSet<Client> Clients { get; set; }
    public required DbSet<Revenue> Revenues { get; set; }
    public required DbSet<SportEvent> SportEvents { get; set; }

    public AppDbContext(DbContextOptions<AppDbContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(
            Assembly.GetExecutingAssembly());
    }
}
