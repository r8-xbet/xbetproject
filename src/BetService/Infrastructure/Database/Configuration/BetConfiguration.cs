﻿using BetService.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BetService.Infrastructure.Database.Configuration;

internal class BetConfiguration : IEntityTypeConfiguration<Bet>
{
    public void Configure(EntityTypeBuilder<Bet> builder)
    {
        builder
            .Property(x => x.Id)
            .ValueGeneratedOnAdd();

        builder
            .HasOne<Client>()
            .WithMany()
            .HasForeignKey(x => x.ClientId);

        builder
            .HasOne<SportEvent>()
            .WithMany()
            .HasForeignKey(x => x.SportEventId);
    }
}
