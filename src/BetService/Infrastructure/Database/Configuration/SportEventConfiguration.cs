﻿using BetService.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BetService.Infrastructure.Database.Configuration;

internal class SportEventConfiguration : IEntityTypeConfiguration<SportEvent>
{
    public void Configure(EntityTypeBuilder<SportEvent> builder)
    {
        builder
            .Property(x => x.Id)
            .ValueGeneratedNever();
    }
}
