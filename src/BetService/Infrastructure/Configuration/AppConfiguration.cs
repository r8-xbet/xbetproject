﻿namespace BetService.Infrastructure.Configuration;

internal static class AppConfiguration
{
    public static string PostgresConnectionString { get; private set; } = null!;
    public static Uri OddsServiceUri { get; private set; } = null!;
    public static Uri SportEventsServiceUri { get; private set; } = null!;

    public static void RegisterSettings(this IServiceCollection services, IConfiguration configuration)
    {
        var postgresConnectionString = configuration.GetConnectionString("Postgres") ?? throw new InvalidOperationException("Connection for DB was not found");
        var sportEventsServiceSettings = configuration.GetSection("SportEventsProvider").Get<SportEventsProviderSettings>() ??
                                         throw new InvalidOperationException($"Cannot find {nameof(SportEventsProviderSettings)}");
        var oddsServiceSettings = configuration.GetSection("OddsService").Get<OddsServiceSettings>() ?? throw new InvalidOperationException($"Cannot find {nameof(OddsServiceSettings)}");
        var rabbitMqSetting = configuration.GetSection("RabbitMq").Get<RabbitMqSetting>() ?? throw new InvalidOperationException($"Not found {nameof(RabbitMqSetting)}");

        PostgresConnectionString = postgresConnectionString;
        OddsServiceUri = oddsServiceSettings.Uri;
        SportEventsServiceUri = sportEventsServiceSettings.Uri;

        services.AddSingleton(rabbitMqSetting);
    }
}
