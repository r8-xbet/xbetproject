﻿namespace BetService.Infrastructure.Configuration;

public class RabbitMqSetting
{
    public string Host { get; init; } = null!;
    public string Queue { get; init; } = null!;
}
