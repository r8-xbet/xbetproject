﻿using BetService.Applications;
using BetService.Infrastructure.Database;
using BetService.Models;
using Microsoft.EntityFrameworkCore;

namespace BetService.Adapters;

public class DbProvider : IDbProvider
{
    private readonly AppDbContext _appDbContext;

    public DbProvider(AppDbContext appDbContext)
    {
        _appDbContext = appDbContext ?? throw new ArgumentNullException(nameof(appDbContext));
    }

    public async Task CreateSportEvents(IList<SportEvent> sportEvents, CancellationToken cancellationToken)
    {
        await _appDbContext.SportEvents.AddRangeAsync(sportEvents, cancellationToken);
        await _appDbContext.SaveChangesAsync(cancellationToken);
    }

    public async Task CreateBet(Bet bet, CancellationToken cancellationToken)
    {
        await _appDbContext.Bets.AddAsync(bet, cancellationToken);
        await _appDbContext.SaveChangesAsync(cancellationToken);
    }

    public Task<List<SportEvent>> GetAllSportEvents(CancellationToken cancellationToken)
    {
        return _appDbContext.SportEvents.ToListAsync(cancellationToken);
    }

    public Task<List<SportEvent>> GetSportEventsAfterTime(DateTime time, CancellationToken cancellationToken)
    {
        return _appDbContext.SportEvents.Where(x => x.Time > time).ToListAsync(cancellationToken);
    }

    public Task<List<SportEvent>> GetSportEventsBeforeTime(DateTime time, CancellationToken cancellationToken)
    {
        return _appDbContext.SportEvents
            .Where(x => !x.IsRevenue)
            .Where(x => x.Time < time)
            .ToListAsync(cancellationToken);
    }

    public Task<SportEvent?> GetSportEvent(long id, CancellationToken cancellationToken)
    {
        return _appDbContext.SportEvents.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
    }

    public Task<Client?> GetClient(string firstName, string secondName, CancellationToken cancellationToken)
    {
        return _appDbContext.Clients.FirstOrDefaultAsync(x => x.FirstName == firstName && x.SurName == secondName, cancellationToken);
    }

    public async Task<Client> CreateClient(Client client, CancellationToken cancellationToken)
    {
        await _appDbContext.Clients.AddAsync(client, cancellationToken);
        await _appDbContext.SaveChangesAsync(cancellationToken);

        return client;
    }

    public Task<List<Bet>> GetBetBySportEventId(long sportEventId, CancellationToken cancellationToken)
    {
        return _appDbContext.Bets
            .Where(bet => bet.SportEventId == sportEventId)
            .Where(bet => !_appDbContext.Revenues.Any(revenue => revenue.BetId == bet.Id))
            .ToListAsync(cancellationToken);
    }

    public async Task<Revenue> CreateRevenue(Revenue revenue, CancellationToken cancellationToken)
    {
        await _appDbContext.Revenues.AddAsync(revenue, cancellationToken);
        await _appDbContext.SaveChangesAsync(cancellationToken);

        return revenue;
    }

    public Task<int> GetClientBetsCountForLastTime(Client client, DateTime time, CancellationToken cancellationToken)
    {
        return _appDbContext.Bets
            .Where(x => x.ClientId == client.Id)
            .Where(x => x.Time > time)
            .CountAsync(cancellationToken);
    }

    public Task<Client?> GetClient(long id, CancellationToken cancellationToken)
    {
        return _appDbContext.Clients.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
    }

    public Task UpdateClient(Client client, CancellationToken cancellationToken)
    {
        return _appDbContext.Clients
            .Where(x => x.Id == client.Id)
            .ExecuteUpdateAsync(setters => setters
                    .SetProperty(x => x.FirstName, client.FirstName)
                    .SetProperty(x => x.SurName, client.SurName)
                    .SetProperty(x => x.ClientType, client.ClientType),
                cancellationToken);
    }
}
