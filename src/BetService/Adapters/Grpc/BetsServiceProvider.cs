﻿using BetsContractsService;
using BetService.Applications;
using BetService.Models;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using OddsContractsService;
using OddsGetRequest = BetsContractsService.OddsGetRequest;
using OddsGetResponse = BetsContractsService.OddsGetResponse;
using SportEvent = BetsContractsService.SportEvent;

namespace BetService.Adapters.Grpc;

public class BetsServiceProvider : BetsApiService.BetsApiServiceBase
{
    private readonly IDbProvider _dbProvider;
    private readonly OddsApiService.OddsApiServiceClient _oddsApiServiceClient;
    private readonly ILogger<BetsServiceProvider> _logger;

    public BetsServiceProvider(IDbProvider dbProvider, OddsApiService.OddsApiServiceClient oddsApiServiceClient, ILogger<BetsServiceProvider> logger)
    {
        _dbProvider = dbProvider ?? throw new ArgumentNullException(nameof(dbProvider));
        _oddsApiServiceClient = oddsApiServiceClient ?? throw new ArgumentNullException(nameof(oddsApiServiceClient));
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
    }

    public override async Task<SportEventsResponse> GetSportEvents(SportEventsGetRequest request, ServerCallContext context)
    {
        _logger.LogInformation("Method {Method} received parameters: {Parameters}", nameof(GetSportEvents), request);

        var sportEvents = await _dbProvider.GetSportEventsAfterTime(request.Time.ToDateTime(), context.CancellationToken);

        var response = new SportEventsResponse
        {
            SportEvents =
            {
                sportEvents.Select(x => new SportEvent
                {
                    Id = x.Id,
                    Time = x.Time.ToTimestamp(),
                    EventName = x.EventName,
                    TeamOne = x.TeamOne,
                    TeamTwo = x.TeamTwo
                })
            }
        };

        _logger.LogInformation("Method {Method} sent response: {Response}", nameof(GetSportEvents), response);

        return response;
    }

    public override async Task<OddsGetResponse> GetOdds(OddsGetRequest request, ServerCallContext context)
    {
        _logger.LogInformation("Method {Method} received parameters: {Parameters}", nameof(GetOdds), request);

        var sportEvent = await _dbProvider.GetSportEvent(request.SportEventId, context.CancellationToken);

        if (sportEvent is null)
            throw new Exception($"Cannot find sport event with id: {request.SportEventId}");

        var oddsGetRequest = new OddsContractsService.OddsGetRequest
        {
            TeamOneName = sportEvent.TeamOne,
            TeamTwoName = sportEvent.TeamTwo,
            SportEventId = request.SportEventId
        };

        var oddsGetResponse = await _oddsApiServiceClient.GetOddsAsync(oddsGetRequest, cancellationToken: context.CancellationToken);

        var response = new OddsGetResponse
        {
            TeamOneName = oddsGetResponse.TeamOneName,
            TeamOneOdd = oddsGetResponse.TeamOneOdd,
            TeamTwoName = oddsGetResponse.TeamTwoName,
            TeamTwoOdd = oddsGetResponse.TeamTwoOdd
        };

        _logger.LogInformation("Method {Method} sent response: {Response}", nameof(GetOdds), response);

        return response;
    }

    public override async Task<ClientGetResponse> GetClient(ClientGetRequest request, ServerCallContext context)
    {
        _logger.LogInformation("Method {Method} received parameters: {Parameters}", nameof(GetClient), request);

        var client = await _dbProvider.GetClient(request.FirstName, request.SurName, context.CancellationToken) ??
                     await _dbProvider.CreateClient(Client.Create(request.FirstName, request.SurName, ClientType.Default), context.CancellationToken);

        var response = new ClientGetResponse
        {
            Id = client.Id,
            FirstName = client.FirstName,
            SurName = client.SurName,
            ClientType = (CLIENT_TYPE)client.ClientType
        };

        _logger.LogInformation("Method {Method} sent response: {Response}", nameof(GetClient), response);

        return response;
    }

    public override async Task<RevenueResponse> GetRevenue(Empty request, IServerStreamWriter<RevenueResponse> responseStream, ServerCallContext context)
    {
        _logger.LogInformation("Method {Method} received ", nameof(GetRevenue));

        while (true)
        {
            var events = await _dbProvider.GetSportEventsBeforeTime(DateTime.UtcNow, context.CancellationToken);

            foreach (var evn in events)
            {
                var bets = await _dbProvider.GetBetBySportEventId(evn.Id, context.CancellationToken);

                foreach (var bet in bets)
                {
                    var revenue = await _dbProvider.CreateRevenue(
                        Revenue.Create(bet.Id, Math.Round(bet.Amount * bet.Odd, 3), DateTime.UtcNow), context.CancellationToken
                    );

                    var revenueResponse = new RevenueResponse
                    {
                        Id = revenue.Id,
                        BetId = revenue.BetId,
                        ClientId = bet.ClientId,
                        Win = (double)revenue.Win,
                        PaymentTime = revenue.PaymentTime.ToTimestamp()
                    };

                    await responseStream.WriteAsync(revenueResponse);

                    evn.ChangeIsRevenue(true);

                    _logger.LogInformation("Method {Method} sent response: {Response}", nameof(GetRevenue), revenueResponse);
                }
            }

            await Task.Delay(TimeSpan.FromSeconds(5));
        }
    }
}
