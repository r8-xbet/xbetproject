﻿using BetService.Applications;
using BetService.Infrastructure.Configuration;
using BetService.Models;
using Contracts;
using Contracts.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace BetService.Adapters;

public class BetConsumerService : BackgroundService
{
    private readonly IServiceScopeFactory _serviceScopeFactory;
    private readonly RabbitMqSetting _rabbitMqSetting;
    private readonly ILogger<BetConsumerService> _logger;

    private readonly IConnection _connection;
    private readonly IModel _channel;

    public BetConsumerService(IServiceScopeFactory serviceScopeFactory, RabbitMqSetting rabbitMqSetting, ILogger<BetConsumerService> logger)
    {
        _serviceScopeFactory = serviceScopeFactory ?? throw new ArgumentNullException(nameof(serviceScopeFactory));
        _rabbitMqSetting = rabbitMqSetting ?? throw new ArgumentNullException(nameof(rabbitMqSetting));
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));

        var factory = new ConnectionFactory { HostName = _rabbitMqSetting.Host };
        _connection = factory.CreateConnection();
        _channel = _connection.CreateModel();
    }

    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("{Service} started", nameof(SportEventsLoaderHostedService));

        try
        {
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += async (ch, ea) =>
            {
                var serializer = new MessageSerializer<BetMessage>();
                var body = serializer.Deserialize(ea.Body.ToArray());

                await using var scope = _serviceScopeFactory.CreateAsyncScope();
                var dbProvider = scope.ServiceProvider.GetService<IDbProvider>();

                var client = await dbProvider.GetClient(body.ClientId, stoppingToken);

                if (client is null)
                    throw new InvalidOperationException($"Cannot find client with id: {body.ClientId}");

                var clientBetsCountForLastTime = await dbProvider.GetClientBetsCountForLastTime(client, DateTime.UtcNow.AddMinutes(-3), stoppingToken);

                if (clientBetsCountForLastTime >= 3)
                    client.SetClientType(ClientType.Cheater);

                await dbProvider.UpdateClient(client, stoppingToken);

                var bet = Bet.Create(body.ClientId, body.SportEventId, body.Odd, body.TeamName, body.Amount, body.Time);
                await dbProvider.CreateBet(bet, stoppingToken);

                _channel.BasicAck(ea.DeliveryTag, false);
            };

            _channel.BasicConsume(_rabbitMqSetting.Queue, false, consumer);
        }
        catch (Exception ex)
        {
            _logger.LogError("{Service} catch an exception with message: {Message}", nameof(SportEventsLoaderHostedService), ex.Message);
        }

        return Task.CompletedTask;
    }

    public override void Dispose()
    {
        _channel.Close();
        _connection.Close();
        base.Dispose();
    }
}
