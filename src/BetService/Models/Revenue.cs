﻿namespace BetService.Models;

public class Revenue
{
    protected Revenue(long betId, decimal win, DateTime paymentTime)
    {
        BetId = betId;
        Win = win;
        PaymentTime = paymentTime;
    }

    /// <summary>
    /// Идентификатор выплаты
    /// </summary>
    public long Id { get; init; }

    /// <summary>
    /// Идентификатор ставки
    /// </summary>
    public long BetId { get; init; }

    /// <summary>
    /// Призовые
    /// </summary>
    public decimal Win { get; init; }

    /// <summary>
    /// Дата и время выплаты
    /// </summary>
    public DateTime PaymentTime { get; init; }

    public static Revenue Create(long betId, decimal win, DateTime paymentTime) => new(betId, win, paymentTime);
}
