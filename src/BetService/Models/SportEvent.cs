﻿namespace BetService.Models;

public class SportEvent
{
    protected SportEvent(long id, string eventName, string teamOne, string teamTwo, DateTime time, bool isRevenue = false)
    {
        Id = id;
        EventName = eventName;
        Time = time;
        TeamOne = teamOne;
        TeamTwo = teamTwo;
        IsRevenue = isRevenue;
    }

    /// <summary>
    /// Идентификатор спортивного события
    /// </summary>
    public long Id { get; init; }

    /// <summary>
    /// Наименование спортивного события
    /// </summary>
    public string EventName { get; set; }

    /// <summary>
    /// Команда 1
    /// </summary>
    public string TeamOne { get; set; }

    /// <summary>
    /// Команда 2
    /// </summary>
    public string TeamTwo { get; set; }

    /// <summary>
    /// Дата и время матча
    /// </summary>
    public DateTime Time { get; set; }

    /// <summary>
    /// Признак совершения выплат по спортивным событиям
    /// </summary>
    public bool IsRevenue { get; set; }

    public static SportEvent Create(long id, string eventName, string teamOne, string teamTwo, DateTime time) =>
        new(id, eventName, teamOne, teamTwo, time);

    public SportEvent ChangeIsRevenue(bool value)
    {
        IsRevenue = value;
        return this;
    }
}
