﻿namespace BetService.Models;

public class Bet
{
    protected Bet(long clientId, long sportEventId, decimal odd, string teamName, decimal amount, DateTime time)
    {
        ClientId = clientId;
        SportEventId = sportEventId;
        Odd = odd;
        Amount = amount;
        Time = time;
        TeamName = teamName;
    }

    public long Id { get; init; }
    public long ClientId { get; init; }
    public long SportEventId { get; init; }
    public decimal Odd { get; init; }
    public string TeamName { get; init; }
    public decimal Amount { get; init; }
    public DateTime Time { get; init; }

    public static Bet Create(long clientId, long sportEventId, decimal odd, string teamName, decimal amount, DateTime time) =>
        new(clientId, sportEventId, odd, teamName, amount, time);
}
