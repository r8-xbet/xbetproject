﻿namespace BetService.Models;

public class Client
{
    protected Client(string firstName, string surName, ClientType clientType)
    {
        FirstName = firstName;
        SurName = surName;
        ClientType = clientType;
    }

    public long Id { get; set; }
    public string FirstName { get; set; }
    public string SurName { get; set; }
    public ClientType ClientType { get; set; }

    public static Client Create(string firstName, string surName, ClientType clientType) =>
        new(firstName, surName, clientType);

    public void SetClientType(ClientType clientType)
    {
        ClientType = clientType;
    }

}
