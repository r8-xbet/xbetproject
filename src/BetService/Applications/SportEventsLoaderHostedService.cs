﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using SportEventsContractsService;
using SportEvent = BetService.Models.SportEvent;

namespace BetService.Applications;

public class SportEventsLoaderHostedService : BackgroundService
{
    private readonly IServiceScopeFactory _serviceScopeFactory;
    private readonly SportEventsApiService.SportEventsApiServiceClient _sportEventsApiServiceClient;
    private readonly ILogger<SportEventsLoaderHostedService> _logger;

    public SportEventsLoaderHostedService(
        IServiceScopeFactory serviceScopeFactory,
        SportEventsApiService.SportEventsApiServiceClient sportEventsApiServiceClient,
        ILogger<SportEventsLoaderHostedService> logger)
    {
        _serviceScopeFactory = serviceScopeFactory ?? throw new ArgumentNullException(nameof(serviceScopeFactory));
        _sportEventsApiServiceClient = sportEventsApiServiceClient ?? throw new ArgumentNullException(nameof(sportEventsApiServiceClient));
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            _logger.LogInformation("{Service} started", nameof(SportEventsLoaderHostedService));

            try
            {
                var streamingCall = _sportEventsApiServiceClient.GetSportEvents(new Empty(), cancellationToken: stoppingToken);

                var responseStream = streamingCall.ResponseStream;

                await foreach (var response in responseStream.ReadAllAsync(cancellationToken: stoppingToken))
                {
                    var content = response.SportEvents;

                    if (content.Count != 0)
                    {
                        await using var scope = _serviceScopeFactory.CreateAsyncScope();
                        var dbProviders = scope.ServiceProvider.GetService<IDbProvider>();

                        if (dbProviders is null)
                            throw new Exception($"Cannot find realization for {nameof(IDbProvider)}");

                        var sportEvents = content.Select(x => SportEvent.Create(x.Id, x.EventName, x.TeamOne, x.TeamTwo, x.Time.ToDateTime())).ToList();

                        await dbProviders.CreateSportEvents(sportEvents, stoppingToken);

                        _logger.LogInformation("{Count} sport events were saved", content.Count);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("{Service} catch an exception with message: {Message}", nameof(SportEventsLoaderHostedService), ex.Message);
            }

            await Task.Delay(5000, stoppingToken);
        }
    }
}
