﻿using BetService.Models;

namespace BetService.Applications;

public interface IDbProvider
{
    /// <summary>
    /// Создание спортивных событий
    /// </summary>
    /// <param name="sportEvents">Коллекция спортивных событий</param>
    /// <param name="cancellationToken">Токен отмены операции</param>
    /// <returns>Task</returns>
    Task CreateSportEvents(IList<SportEvent> sportEvents, CancellationToken cancellationToken);

    /// <summary>
    /// Создание ставки
    /// </summary>
    /// <param name="bet">Ставка</param>
    /// <param name="cancellationToken">Токен отмены операции</param>
    /// <returns>Task</returns>
    Task CreateBet(Bet bet, CancellationToken cancellationToken);

    /// <summary>
    /// Получение всех спортивных событий
    /// </summary>
    /// <param name="cancellationToken">Токен отмены операции</param>
    /// <returns>Task</returns>
    Task<List<SportEvent>> GetAllSportEvents(CancellationToken cancellationToken);

    /// <summary>
    /// Получение спортивных событий после указанного времени
    /// </summary>
    /// <param name="time">Дата и время</param>
    /// <param name="cancellationToken">Токен отмены операции</param>
    /// <returns>Спортивные события после указанного времени</returns>
    Task<List<SportEvent>> GetSportEventsAfterTime(DateTime time, CancellationToken cancellationToken);

    /// <summary>
    /// Получение спортивных событий до указанного времени
    /// </summary>
    /// <param name="time">Дата и время</param>
    /// <param name="cancellationToken">Токен отмены операции</param>
    /// <returns>Спортивные события до указанного времени</returns>
    Task<List<SportEvent>> GetSportEventsBeforeTime(DateTime time, CancellationToken cancellationToken);

    /// <summary>
    /// Получение спортивного события
    /// </summary>
    /// <param name="id">Идентификатор спортивного события</param>
    /// <param name="cancellationToken">Токен отмены операции</param>
    /// <returns>Спортивное событие</returns>
    Task<SportEvent?> GetSportEvent(long id, CancellationToken cancellationToken);

    /// <summary>
    /// Получение клиента
    /// </summary>
    /// <param name="firstName">Фамилия</param>
    /// <param name="secondName">Имя</param>
    /// <param name="cancellationToken">Токен отмены операции</param>
    /// <returns>Клиент</returns>
    Task<Client?> GetClient(string firstName, string secondName, CancellationToken cancellationToken);

    /// <summary>
    /// Создание клиента
    /// </summary>
    /// <param name="client">Клиент</param>
    /// <param name="cancellationToken">Токен отмены операции</param>
    /// <returns>Клиент</returns>
    Task<Client> CreateClient(Client client, CancellationToken cancellationToken);

    /// <summary>
    /// Получение ставок по спортивному событию
    /// </summary>
    /// <param name="sportEventId">Идентификатор спортивного события</param>
    /// <param name="cancellationToken">Токен отмены операции</param>
    /// <returns>Ставки</returns>
    Task<List<Bet>> GetBetBySportEventId(long sportEventId, CancellationToken cancellationToken);

    /// <summary>
    /// Создание записи об выплате
    /// </summary>
    /// <param name="revenue">Выплата</param>
    /// <param name="cancellationToken">Токен отмены операции</param>
    /// <returns>Выплата</returns>
    Task<Revenue> CreateRevenue(Revenue revenue, CancellationToken cancellationToken);

    Task<int> GetClientBetsCountForLastTime(Client client, DateTime time, CancellationToken cancellationToken);

    Task<Client?> GetClient(long id, CancellationToken cancellationToken);

    Task UpdateClient(Client client, CancellationToken cancellationToken);
}
