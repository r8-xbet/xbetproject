﻿namespace OddsService.Infrastructure.Configuration;

internal static class AppConfiguration
{
    public static Uri SportEventsServiceUri { get; private set; } = null!;

    public static void RegisterSettings(this IServiceCollection services, IConfiguration configuration)
    {
        var sportEventsServiceSettings = configuration.GetSection("SportEventsProvider").Get<SportEventsProviderSettings>() ??
                                         throw new InvalidOperationException($"Cannot find {nameof(SportEventsProviderSettings)}");

        SportEventsServiceUri = sportEventsServiceSettings.Uri;
    }
}
