﻿using OddsService.Adapters;
using OddsService.Adapters.Grpc;
using OddsService.Applications;
using OddsService.Infrastructure.Configuration;
using SportEventsContractsService;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddHostedService<SportEventsLoaderHostedService>();
builder.Services.AddHostedService<OddsUpdaterHostedService>();

builder.Services.RegisterSettings(builder.Configuration);

builder.Services.AddGrpcClient<SportEventsApiService.SportEventsApiServiceClient>(o =>
{
    o.Address = AppConfiguration.SportEventsServiceUri;
});

builder.Services.AddSingleton<SportEventsMemoryProvider>();

builder.Services.AddGrpc();
builder.Services.AddGrpcReflection();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.MapGrpcReflectionService();
}

app.MapGrpcService<OddsServiceProvider>();

await app.RunAsync();
