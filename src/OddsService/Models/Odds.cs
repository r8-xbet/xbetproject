﻿namespace OddsService.Models;

public record Odds(double TeamOneOdds, double TeamTwoOdds);
