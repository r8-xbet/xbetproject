﻿using OddsService.Adapters;
using OddsService.Models;

namespace OddsService.Applications;

public class OddsUpdaterHostedService : BackgroundService
{
    private readonly SportEventsMemoryProvider _sportEventsMemoryProvider;
    private readonly ILogger<OddsUpdaterHostedService> _logger;

    public OddsUpdaterHostedService(
        SportEventsMemoryProvider sportEventsMemoryProvider,
        ILogger<OddsUpdaterHostedService> logger)
    {
        _sportEventsMemoryProvider = sportEventsMemoryProvider ?? throw new ArgumentNullException(nameof(sportEventsMemoryProvider));
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("{Service} started", nameof(OddsUpdaterHostedService));

        while (!stoppingToken.IsCancellationRequested)
        {
            var allOdds = _sportEventsMemoryProvider.GetAllOdds();

            var newOdds = new Dictionary<long, Odds>();

            foreach (var keyValuePair in allOdds)
            {
                var odds = new Odds(Random.Shared.NextDouble() * 3 + 1, Random.Shared.NextDouble() * 3 + 1);
                newOdds.Add(keyValuePair.Key, odds);
            }

            _sportEventsMemoryProvider.UpdateOdd(newOdds);

            await Task.Delay(1000, stoppingToken);
        }
    }
}
