﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using OddsService.Adapters;
using OddsService.Models;
using SportEventsContractsService;

namespace OddsService.Applications;

public class SportEventsLoaderHostedService : BackgroundService
{
    private readonly SportEventsMemoryProvider _sportEventsMemoryProvider;
    private readonly SportEventsApiService.SportEventsApiServiceClient _sportEventsApiServiceClient;
    private readonly ILogger<SportEventsLoaderHostedService> _logger;

    public SportEventsLoaderHostedService(
        SportEventsMemoryProvider sportEventsMemoryProvider,
        SportEventsApiService.SportEventsApiServiceClient sportEventsApiServiceClient,
        ILogger<SportEventsLoaderHostedService> logger)
    {
        _sportEventsMemoryProvider = sportEventsMemoryProvider ?? throw new ArgumentNullException(nameof(sportEventsMemoryProvider));
        _sportEventsApiServiceClient = sportEventsApiServiceClient ?? throw new ArgumentNullException(nameof(sportEventsApiServiceClient));
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("{Service} started", nameof(SportEventsLoaderHostedService));

        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                var streamingCall = _sportEventsApiServiceClient.GetSportEvents(new Empty(), cancellationToken: stoppingToken);

                var responseStream = streamingCall.ResponseStream;

                await foreach (var response in responseStream.ReadAllAsync(cancellationToken: stoppingToken))
                {
                    var content = response.SportEvents;

                    if (content.Count != 0)
                    {
                        foreach (var sportEvent in content)
                        {
                            var odds = new Odds(Random.Shared.NextDouble() * 3 + 1, Random.Shared.NextDouble() * 3 + 1);
                            _sportEventsMemoryProvider.AddOdds(sportEvent.Id, odds);
                        }

                        _logger.LogInformation("{Count} sport events were saved", content.Count);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("{Service} catch an exception with message: {Message}", nameof(SportEventsLoaderHostedService), ex.Message);
            }

            await Task.Delay(5000, stoppingToken);
        }
    }
}
