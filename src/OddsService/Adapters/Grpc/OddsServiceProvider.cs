﻿using Grpc.Core;
using OddsContractsService;

namespace OddsService.Adapters.Grpc;

public class OddsServiceProvider : OddsApiService.OddsApiServiceBase
{
    private readonly SportEventsMemoryProvider _sportEventsMemoryProvider;
    private readonly ILogger<OddsServiceProvider> _logger;

    public OddsServiceProvider(SportEventsMemoryProvider sportEventsMemoryProvider, ILogger<OddsServiceProvider> logger)
    {
        _sportEventsMemoryProvider = sportEventsMemoryProvider ?? throw new ArgumentNullException(nameof(sportEventsMemoryProvider));
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
    }

    public override Task<OddsGetResponse> GetOdds(OddsGetRequest request, ServerCallContext context)
    {
        _logger.LogInformation("Method {Method} received parameters: {Parameters}", nameof(GetOdds), request);

        var odds = _sportEventsMemoryProvider.GetOdds(request.SportEventId);

        var response = new OddsGetResponse
        {
            TeamOneName = request.TeamOneName,
            TeamOneOdd = odds.TeamOneOdds,// Random.Shared.NextDouble() * 3 + 1,
            TeamTwoName = request.TeamTwoName,
            TeamTwoOdd = odds.TeamTwoOdds,//Random.Shared.NextDouble() * 3 + 1
        };

        _logger.LogInformation("Method {Method} sent response: {Response}", nameof(GetOdds), response);

        return Task.FromResult(response);
    }
}
