﻿using OddsService.Models;

namespace OddsService.Adapters;

public class SportEventsMemoryProvider
{
    private Dictionary<long, Odds> _cash = new();

    private readonly object _lockObject = new();

    public void AddOdds(long sportEventId, Odds odds)
    {
        lock (_lockObject)
        {
            _cash.TryAdd(sportEventId, odds);
        }
    }

    public Odds GetOdds(long id)
    {
        lock (_lockObject)
        {
            if (_cash.TryGetValue(id, out var odds))
            {
                return odds;
            }

            throw new InvalidOperationException($"Cannot find sport event with id: {id}");
        }
    }

    public Dictionary<long, Odds> GetAllOdds()
    {
        lock (_lockObject)
        {
            return _cash;
        }
    }

    public void UpdateOdd(Dictionary<long, Odds> odds)
    {
        lock (_lockObject)
        {
            _cash = odds;
        }
    }
}
