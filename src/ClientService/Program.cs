﻿using BetsContractsService;
using ClientService.Application;
using ClientService.Infrastructure.Configuration;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddHostedService<BetterHostedService>();
builder.Services.AddHostedService<RevenueLoaderHostedService>();

builder.Services.RegisterSettings(builder.Configuration);

builder.Services.AddGrpcClient<BetsApiService.BetsApiServiceClient>(o =>
{
    o.Address = AppConfiguration.BetsServiceUri;
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

await app.RunAsync();
