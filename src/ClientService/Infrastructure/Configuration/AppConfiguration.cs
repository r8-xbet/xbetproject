﻿namespace ClientService.Infrastructure.Configuration;

public static class AppConfiguration
{
    public static Uri BetsServiceUri { get; private set; } = null!;

    public static void RegisterSettings(this IServiceCollection services, IConfiguration configuration)
    {
        var betServiceSetting = configuration.GetSection("BetService").Get<BetServiceSetting>() ?? throw new InvalidOperationException($"Not found {nameof(BetServiceSetting)}");
        var rabbitMqSetting = configuration.GetSection("RabbitMq").Get<RabbitMqSetting>() ?? throw new InvalidOperationException($"Not found {nameof(RabbitMqSetting)}");

        BetsServiceUri = betServiceSetting.Uri;

        services.AddSingleton(rabbitMqSetting);
    }
}
