﻿namespace ClientService.Infrastructure.Configuration;

public class RabbitMqSetting
{
    public string Host { get; init; } = null!;
    public string Exchange { get; init; } = null!;
}
