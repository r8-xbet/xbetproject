﻿using BetsContractsService;
using ClientService.Infrastructure.Configuration;
using Contracts;
using Contracts.Models;
using Google.Protobuf.WellKnownTypes;
using RabbitMQ.Client;

namespace ClientService.Application;

public class BetterHostedService : BackgroundService
{
    private readonly BetsApiService.BetsApiServiceClient _betsApiServiceClient;
    private readonly RabbitMqSetting _rabbitMqSetting;
    private readonly ILogger<BetterHostedService> _logger;

    readonly string[] _firstNames = ["Пётр", "Иван", "Игнат", "Василий"];
    readonly string[] _surNames = ["Петров", "Иванов", "Кузнецов", "Кактусов"];

    public BetterHostedService(BetsApiService.BetsApiServiceClient betsApiServiceClient, RabbitMqSetting rabbitMqSetting, ILogger<BetterHostedService> logger)
    {
        _betsApiServiceClient = betsApiServiceClient ?? throw new ArgumentNullException(nameof(betsApiServiceClient));
        _rabbitMqSetting = rabbitMqSetting ?? throw new ArgumentNullException(nameof(rabbitMqSetting));
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
    }

    protected override async Task ExecuteAsync(CancellationToken cancellationToken)
    {
        var nameService = nameof(BetterHostedService);

        _logger.LogInformation("{Service} started", nameService);

        while (!cancellationToken.IsCancellationRequested)
        {
            try
            {
                var sportEventsGetRequest = new SportEventsGetRequest { Time = DateTime.UtcNow.ToTimestamp() };
                var sportEventsResponse = await _betsApiServiceClient.GetSportEventsAsync(sportEventsGetRequest, cancellationToken: cancellationToken);

                var sportEvents = sportEventsResponse.SportEvents;

                _logger.LogInformation("{Count} sport events received", sportEvents.Count);
                if (sportEvents.Count != 0)
                {
                    var sportEvent = sportEvents[new Random().Next(0, sportEvents.Count)];

                    var firstName = _firstNames[Random.Shared.Next(_firstNames.Length)];
                    var surName = _surNames[Random.Shared.Next(_surNames.Length)];

                    var clientGetRequest = new ClientGetRequest
                    {
                        FirstName = firstName,
                        SurName = surName
                    };

                    var client = await _betsApiServiceClient.GetClientAsync(clientGetRequest, cancellationToken: cancellationToken);

                    var oddsGetRequest = new OddsGetRequest { SportEventId = sportEvent.Id };
                    var odds = await _betsApiServiceClient.GetOddsAsync(oddsGetRequest, cancellationToken: cancellationToken);

                    SendMessage(client.Id, odds.TeamOneOdd, odds.TeamOneName, sportEvent.Id);

                    _logger.LogInformation("Bet was accepted");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("{Service} catch an exception with message: {Message}", nameService, ex.Message);
            }

            await Task.Delay(5000, cancellationToken);
        }
    }

    private void SendMessage(long clientId, double odds, string team, long sportEventId)
    {
        var factory = new ConnectionFactory
        {
            HostName = _rabbitMqSetting.Host
        };

        using var connection = factory.CreateConnection();

        using var channel = connection.CreateModel();

        var random = new Random();

        var message = new BetMessage(
            clientId,
            sportEventId,
            (decimal)odds,
            team,
            random.Next(100, 10000),
            DateTime.UtcNow
        );

        var serializer = new MessageSerializer<BetMessage>();
        var body = serializer.Serialize(message);

        channel.BasicPublish(
            exchange: _rabbitMqSetting.Exchange,
            routingKey: "",
            mandatory: true,
            body: body);
    }
}
