﻿using BetsContractsService;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;

namespace ClientService.Application;

public class RevenueLoaderHostedService : BackgroundService
{
    private readonly BetsApiService.BetsApiServiceClient _betsApiServiceClient;
    private readonly ILogger<RevenueLoaderHostedService> _logger;

    public RevenueLoaderHostedService(
        BetsApiService.BetsApiServiceClient betsApiServiceClient,
        ILogger<RevenueLoaderHostedService> logger)
    {
        _betsApiServiceClient = betsApiServiceClient ?? throw new ArgumentNullException(nameof(betsApiServiceClient));
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            _logger.LogInformation("{Service} started", nameof(RevenueLoaderHostedService));

            try
            {
                var streamingCall = _betsApiServiceClient.GetRevenue(new Empty(), cancellationToken: stoppingToken);

                var responseStream = streamingCall.ResponseStream;

                await foreach (var response in responseStream.ReadAllAsync(cancellationToken: stoppingToken))
                {
                    _logger.LogInformation("Client {Client} received {Amount} money", response.ClientId, response.Win);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("{Service} catch an exception with message: {Message}", nameof(RevenueLoaderHostedService), ex.Message);
            }

            await Task.Delay(5000, stoppingToken);
        }
    }
}
